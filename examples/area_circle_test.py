import math
from garuda import *

def area(radius):
    return math.pow(radius, 2) * math.pi


with TestSuite() as ts:
    @test(ts)
    def area_circle_with_radius_5_gives_result():
        assert_equals(78.5398, area(5), precision=4)


    @test(ts)
    def area_circle_with_radius_2_gives_result():
        assert_equals(12.56637061, area(2), precision=8)


    @test(ts)
    def area_circle_with_radius_2_gives_result():
        assert_equals(12.56637061, area(2), precision=9)


    @test(ts)
    def precision():
        assert_equals(2.001, 2, precision=15)
