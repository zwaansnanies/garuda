from garuda import *


kv = {"one": 1, "two": 2, "three": [3]}
kv_ref = {"two": 2, "one": 1, "three": 3}

with TestSuite() as ts:

    @test(ts)
    def map_test():
        assert_equals(kv_ref, kv)
