from garuda import *

tuple_1 = (1, 'a', [1,2,3])

tuple_ref = (1, 2)


with TestSuite() as ts:

    @test(ts)
    def tuple_test():
        assert_equals(tuple_ref, tuple_1)

