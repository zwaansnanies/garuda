from garuda import *


def is_prime(n):
    return n > 1 and all(n % i != 0 for i in range(2, n))


def is_prime_sol(n):
    return n > 1 and all(n % i != 0 for i in range(2, n))


with TestSuite() as ts:
    with TestGroup(ts, name="is_prime", score=100, strategy=Cumulative) as tg:
        @test(tg)
        def check_primes_from_output_file():
            compare_output_from_file(input_file="input", output_file="output", function=is_prime)


        @test(tg)
        def check_primes_from_function_solution():
            compare_output_from_function(input_file="input", solution_function=is_prime_sol, function=is_prime)
