import sys

if len(sys.argv) > 1:
    x = int(sys.argv[1])
else:
    x = 9001


def is_prime(n):
    return n > 1 and all(n % i != 0 for i in range(2, n))


with open("input", "w+") as f:
    for x in range(x):
        f.write(str(x) + "\n")

with open("input", "r") as f2:
    with open("output", "w+") as f:
        lines = f2.readlines()
        for x in lines:
            f.write(str(is_prime(int(x))) + "\n")
