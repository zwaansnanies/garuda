from garuda import *


def square(x):
    return x * x


with TestSuite() as ts:
    with TestGroup(ts, name="square_tests", strategy=Cumulative) as tg:
        @test(tg)
        def square_of_0_should_return_0():
            assert_equals(0, square(0))


        @test(tg)
        def square_of_1_should_return_1():
            assert_equals(1, square(1))


        @test(tg)
        def square_of_2_should_return_4():
            assert_equals(4, square(2))


        @test(tg)
        def square_of_3_should_return_9():
            assert_equals(9, square(3))


        @test(tg)
        def square_of_4_should_return_16():
            assert_equals(16, square(4))


        @test(tg)
        def square_of_5_should_return_25():
            assert_true(square(5) == 25)
