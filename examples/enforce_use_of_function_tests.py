from garuda import *
from examples.enforce_use_of_function_student import area_circle, volume_cylinder, calculate_circumference

with TestSuite() as ts:
    with TestGroup(suite=ts, name="required_tests") as tg:
        @test(tg)
        def volume_cylinder_uses_area_circle():
            assert_uses_function(function=volume_cylinder, function_to_use=area_circle, args=(10, 15))


        @test(tg)
        def volume_cylinder_failing_uses_calculate_circumference():
            assert_uses_function(function=volume_cylinder, function_to_use=calculate_circumference, args=(10, 15))
