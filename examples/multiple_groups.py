from garuda import *


def is_prime(n):
    return n > 1 and all(n % i != 0 for i in range(2, n))


def square(x):
    return x * x


def sum(x, y):
    return x + y


with TestSuite() as ts:
    with TestGroup(ts, name="another_testgroup", score=3) as tg:
        @test(tg)
        def square_of_4_should_return_16():
            assert_equals(16, square(5))


        @test(tg)
        def number_97_is_a_prime():
            assert_true(is_prime(97))


        @test(tg)
        def number_2017_is_a_prime():
            assert_true(is_prime(2017))

    with TestGroup(ts, name="square_tests", score=2) as tg:
        @test(tg)
        def square_of_4_should_return_16():
            assert_equals(16, square(4))


        @test(tg, ignore=True)
        def square_of_5_should_return_25():
            assert_true(square(5) == 25)

    with TestGroup(ts, name="sum_tests", ignore=True) as tg:
        @test(tg)
        def sum_of_1_and_1_should_return_2():
            assert_equals(2, sum(1, 1))


        @test(tg)
        def sum_of_3_and_2_should_return_5():
            assert_equals(5, sum(3, 2))


        @test(tg)
        def sum_of_7_and_4_should_return_11():
            assert_equals(11, sum(7, 4))
