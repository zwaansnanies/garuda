from garuda import *

set_1 = {1, 2, 3}

ref = {1, 2, 1, 1, 5, 6}


with TestSuite() as ts:

    @test(ts)
    def set_test():
        assert_equals(set_1, ref)
