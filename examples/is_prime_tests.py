from garuda import *

def is_prime_student(string):
    return "hello" + string


def is_prime(n):
    return n > 1 and all(n % i != 0 for i in range(2, n))



with TestSuite() as ts:
    with TestGroup(ts, name="is_prime", score=100, ignore=False, strategy=Cumulative) as tg:
        @test(tg, time_out=5, ignore=True)
        def number_1_is_no_prime():
            assert_false(is_prime(1))


        @test(tg, essential=True)
        def number_2_is_a_prime():
            assert_true(is_prime(2))


        @test(tg, essential=True)
        def number_15_is_no_prime():
            assert_false(is_prime(11))


        @test(tg, essential=True)
        def number_17_is_a_prime():
            assert_true(is_prime(17))


        @test(tg, essential=True)
        def number_19_is_a_prime():
            assert_true(is_prime(19))


        @test(tg, essential=True)
        def number_97_is_a_prime():
            assert_true(is_prime(97))


        @test(tg, essential=True)
        def number_2017_is_a_prime():
            assert_true(is_prime(2017))


        for i in range(10):
            @test(tg, name=f'test_student_{i}')
            def test_student():
                # assert type(i) == str
                assert_equals(is_prime(i), is_prime_student(i))