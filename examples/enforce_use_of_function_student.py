import math


def area_circle(r):
    return math.pi * math.pow(r, 2)


def volume_cylinder(r, h):
    return area_circle(r) * h


def calculate_circumference():
    pass
