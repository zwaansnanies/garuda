from garuda import *


def fibonacci_recursive(n):
    if n < 2:
        return 1
    return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)


def fibonacci_iterative(n):
    x = 1
    y = 1
    for _ in range(n):
        x, y = y, x + y
    return x


with TestSuite() as ts:
    with TestGroup(ts, name="fibonacci_tests", score=4, strategy=Cumulative) as tg:
        @test(tg, time_out=3, ignore=True)
        def number_4_of_fibonacci_recursive_returns_5():
            assert_equals(5, fibonacci_recursive(4))


        @test(tg, time_out=3)
        def number_34_of_fibonacci_recursive_returns_9227465():
            assert_equals(9227465, fibonacci_recursive(34))


        @test(tg, time_out=3)
        def number_4_of_fibonacci_iterative_returns_5():
            assert_equals(5, fibonacci_iterative(4))


        @test(tg, time_out=3)
        def number_34_of_fibonacci_iterative_returns_9227465():
            assert_equals(9227465, fibonacci_iterative(34))
