from garuda import *

d_arr = []
for i in range(10):
    d_arr.append([j for j in range(i)])

ref_1 = [[0], [0], [0, 1, 2], [0, 1, 2, 3]]
ref_2 = [[0], [0, 1, 2], [0, 1, 2], [0, 1, 2, 3]]
ref_3 = [1, [0,2], 4, [7, 8, 9, 3]]
tested = [[0], [0, 1], [0, 1, 2], [0, 1, 2, 3]]

s_arr = ['1', 2]
s_tested = [1, '2']

def student():
    return "hello"

def sol():
    return 12

with TestSuite() as ts:
    with TestGroup(ts, name="Double array test") as tg:

        @test(tg)
        def double_array():
            assert_equals(ref_1, tested)

        @test(tg)
        def double_array_2():
            assert_equals(ref_2, tested)

        @test(tg)
        def double_array_3():
            assert_equals(ref_3, tested)

    with TestGroup(ts, name="Single array test") as tg:

        @test(tg)
        def single_array():
            assert_equals(s_arr, s_tested)

    with TestGroup(ts, name="Different return type") as tg:

        @test(tg)
        def test_different_return_type():
            assert_equals(student(), sol())

