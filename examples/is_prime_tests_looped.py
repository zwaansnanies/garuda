from garuda import *


def is_prime(n):
    return n > 1 and all(n % i != 0 for i in range(2, n))


def is_prime_uncomplete(n):
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


with TestSuite() as ts:
    with TestGroup(ts, name="is_prime", score=100, strategy=Cumulative) as tg:
        for i in range(5000):
            @test(tg, name=f'check whether {i} is prime')
            def prime_tests():
                assert_equals(is_prime(i), is_prime_uncomplete(i))
