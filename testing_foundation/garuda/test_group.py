import functools
from garuda import GroupResult, Score, Status, TestError, AllOrNothing
from garuda.config import ConfigReader


class TestGroup:
    """
    A class representing a group of tests.
    """

    def __init__(self, suite, **kwargs):
        """
        Args:
            suite: an instance of type TestSuite.
            **kwargs: (optional).
        """

        self.test_results = []
        self.suite = suite

        self.name = kwargs
        self.ignore_all = kwargs

        self.score = kwargs
        self.strategy = kwargs

        suite.add_test_group(self)

    @property
    def name(self):
        """
        Raises: TestError if name attribute is not of type string.
        Returns: a string representing the name of instance of type TestGroup.
        """

        return self.__name

    @name.setter
    def name(self, kwargs):
        if "name" in kwargs:
            self.__name = kwargs["name"]
            if not isinstance(self.__name, str):
                raise TestError("Please assign a name of type string to your testgroup.")
            if self.__name.strip() == "":
                raise TestError("Please assign a non empty string to your testgroup")
        else:
            raise TestError("Please assign a name to your testgroup.")

    @property
    def ignore_all(self):
        """
        Raises: TestError if ignore_all attribute is not of type boolean.
        Returns: a boolean representing if all tests in the TestGroup instance are ignored.
        """

        return self.__ignore_all

    @ignore_all.setter
    def ignore_all(self, kwargs):
        if "ignore" in kwargs:
            ignore = kwargs["ignore"]
            if not isinstance(ignore, bool):
                raise TestError("An ignore value should be a boolean.")
            self.__ignore_all = ignore

        else:
            self.__ignore_all = False

    @property
    def score(self):
        """
        Raises: TestError if score attribute is not of type integer or value is smaller than zero.
        Returns: an instance of type Score.
        """

        return self.__score

    @score.setter
    def score(self, kwargs):
        if "score" in kwargs:
            score = kwargs["score"]
            if not isinstance(score, int):
                raise TestError("The assigned score to your testgroup should be an int.")
            if score < 0:
                raise TestError("The assigned score cannot be negative.")
            self.__score = score
        else:
            self.__score = ConfigReader.default_score

    @property
    def strategy(self):
        """
        Returns: an instance of type ScoringStrategy.
        Raises: TestError if strategy attribute is not of type ScoringStrategy.
        """

        return self.__strategy

    @strategy.setter
    def strategy(self, kwargs):
        if "strategy" in kwargs:
            try:
                strategy = kwargs["strategy"](self)
                self.__strategy = strategy
            except Exception:
                raise TestError("The assigned strategy should be of type ScoringStrategy.")
        else:
            self.__strategy = AllOrNothing(self)

    def count_tests(self):
        """
        Returns: an integer representing the number of tests in the TestGroup-instance.
        """

        return len(self.test_results)

    @property
    def passed(self):
        """
        Returns: an integer representing the number of tests in the TestGroup-instance that passed.
        """

        return functools.reduce((lambda acc, result: acc + 1 if result.status is Status.PASS else acc),
                                self.test_results, 0)

    @property
    def failed(self):
        """
        Returns: an integer representing the number of tests in the TestGroup-instance that failed.
        """

        return functools.reduce(
            (lambda acc, result: acc + 1 if result.status in (Status.FAIL, Status.TIMEOUT) else acc),
            self.test_results, 0)

    @property
    def ignored(self):
        """
        Returns: an integer representing the number of tests in the TestGroup-instance that are ignored.
        """

        return functools.reduce((lambda acc, result: acc + 1 if result.status is Status.IGNORED else acc),
                                self.test_results,
                                0)

    @property
    def skipped(self):
        """
        Returns: an integer representing the number of tests in the TestGroup-instance that are skipped.
        """

        return functools.reduce((lambda acc, result: acc + 1 if result.status is Status.SKIPPED else acc),
                                self.test_results,
                                0)

    @property
    def has_failed_essential(self):
        """
        Returns: a boolean representing if any essential test in the TestGroup-instance has failed.
        """

        return any(list(map((lambda result: result.status is Status.FAIL and result.essential), self.test_results)))

    def __calculate_score(self):
        return self.strategy()

    def __calculate_time(self):
        time = 0.0
        for result in self.test_results:
            time += result.seconds
        return round(time, 5)

    def add_test_result(self, test_result):
        """
        Args:
            test_result: an instance of type TestResult.

        Returns: void
        """

        self.test_results.append(test_result)

    def __call__(self, *args, **kwargs):
        """
        Args:
            *args: (optional).
            **kwargs: (optional).

        Returns: an instance of type GroupResult.
        """

        calculated_score = self.__calculate_score()

        return GroupResult(self.count_tests(), self.passed,
                           self.failed,
                           self.ignored,
                           self.skipped,
                           self.__calculate_time(),
                           Score(calculated_score, self.score))

    def __repr__(self):
        output = []
        """
        Returns: a list containing TestResult instances
        """
        for result in self.test_results:
            output.append(result.__repr__())
        return output

    def __enter__(self):
        """
        A method to create a context manager for instance TestGroup.
        Returns: a context manager for instance TestGroup.
        """

        return self

    def __exit__(self, type, value, traceback):
        """
        A method to destroy a context manager for instance TestGroup.
        """

        pass
