from garuda import TestError, TestGroup, OverallResult
from garuda.logger import *
import functools


class TestSuite:
    """
    A class representing a suite of tests which are a collection of TestGroup instances.
    """

    def __init__(self, *_ignored, **kwargs):
        """
        Args:
            *ignored: nothing
            **kwargs: a dictionary containing the attributes to be set in an instance of type TestSuite.
        """

        self.test_groups = []
        if "filename" in kwargs:
            self.filename = kwargs["filename"]
        else:
            self.filename = None
        self.group_results = []

    def __add_group_result(self, group_result):
        self.group_results.append(group_result)

    def add_test_result(self, test_result):
        """
        A method to add a TestResult-instance without a TestGroup to a TestSuite-instance.

        Args:
            test_result: an instance of type TestResult.

        Returns: void
        """

        group_individual_tests = self.__get_group_individual_tests("Individual examples")
        if group_individual_tests is None:
            group_individual_tests = TestGroup(self, name="Individual examples")
        group_individual_tests.add_test_result(test_result)

    def __enter__(self):
        """
        A method to create a context manager for a TestSuite-instance.
        Returns:  a context manager for a TestSuite-instance.
        """

        ConfigReader.read()
        return self

    def __exit__(self, type, value, traceback):
        """
        A method to destroy a context manager for a TestSuite-instance.
        Returns: void
        """

        if type is TestError:
            return

        for group in self.test_groups:
            group_result = group()
            self.__add_group_result(group_result)

        self()

    def __calculate_overall_result(self):
        return OverallResult(functools.reduce(lambda total, result: total + result,
                                              self.group_results))

    def __get_group_individual_tests(self, name: str):
        return next(filter(lambda group: group.name == name, self.test_groups), None)

    def add_test_group(self, test_group):
        """
        A method to add a TestGroup-instance to a TestSuite-instance.

        Args:
            test_group: an instance of type TestGroup.

        Returns: void
        """

        self.test_groups.append(test_group)

    def __call__(self, *args, **kwargs):
        """
        A method to override the call brackets '()' which sends all GroupResult-instances and corresponding TestGroup-
        instances as LogElement-instances to an instance of LoggerTracker.

        Args:
            *args: (optional).
            **kwargs: (optional).

        Returns: void
        """

        with LoggerTracker() as logger:
            for group_result, test_group in zip(self.group_results, self.test_groups):
                logger.send_log_element(LogElement(group_result, test_group))

            logger.send_overall_result(self.__calculate_overall_result())
