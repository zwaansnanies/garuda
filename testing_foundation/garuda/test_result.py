from garuda import TestError, Status
from garuda.visualizers import visualize


class ExpectedActual:
    """
    A class representing the expected and actual values of a test.
    """

    def __init__(self, expected, actual):
        """
        Args:
            expected: an object representing the expected value of a test.
            actual: an object representing the actual value of a test.
        """

        self.expected = expected
        self.actual = actual

    def __str__(self):
        """
        Returns: a string representation of an instance of type ExpectedActual.
        """

        return visualize(self.expected, self.actual)

    def __repr__(self):
        """
        Returns: a dictionary containing the attributes of an instance of type ExpectedActual.
        """

        return {"expected": self.expected, "actual": self.actual}


class TestResult:
    """
    A class representing the result of a test that has been run.
    """

    def __init__(self, name: str, status: Status, seconds: float,
                 expected_actual: ExpectedActual = None,
                 essential: bool = False,
                 message: str = ''):
        """
        Args:
            name: a string representing the name of a test.
            status: an enum value of class Status representing the status of a test.
            seconds: a float representing the amount of seconds to determine the test result.
            expected_actual: an instance of type ExpectedActual to represent the expected and actual values of a test.
            essential: a boolean to represent if a test is essential.
            message: a string representing the constructive feedback message when a test fails.
        """

        self.name = name
        self.status = status
        self.seconds = seconds
        self.expected_actual = expected_actual
        self.essential = essential
        self.message = message

    @staticmethod
    def default_ignored(test_name):
        """
        Args:
            test_name: a string representing the name af a test.

        Returns: a default instance of TestResult with attribute status set to ignored and attribute essential set to false.
        """

        return TestResult(test_name, Status.IGNORED, 0, essential=False)

    def __str__(self):
        """
        Returns: a formatted string representing an instance of type TestResult.
        """

        result = ""
        if self.status is Status.PASS:
            result += "PASS:      {}{}".format(self.__name, "!" if self.essential else "")
        elif self.status is Status.FAIL:
            result += "FAIL:      {}{} {}".format(self.__name, "!" if self.essential else "", self.expected_actual
            if self.expected_actual is not None else '')
        elif self.status is Status.TIMEOUT:
            result += "FAIL:      {}".format(self.__name)
        elif self.status is Status.IGNORED:
            result += "IGNORED:   {}".format(self.__name)
        elif self.status is Status.SKIPPED:
            result += "SKIPPED:   {}".format(self.__name)

        if self.message != '':
            result += ' ({})'.format(self.message)
        return result

    @property
    def name(self):
        """
        Returns: a string representing the name of a test.
        Raises: TestError if type is not string.
        """

        return self.__name

    @name.setter
    def name(self, name):
        if not isinstance(name, str):
            raise TestError("Please assign a name of type string to TestResult.")
        self.__name = name

    @property
    def status(self):
        """
        Returns: an instance of enum type Status.
        Raises: TestError is type is not Status.
        """

        return self.__status

    @status.setter
    def status(self, status):
        if not isinstance(status, Status):
            raise TestError("Please assign a status of type enum Status to TestResult.")
        self.__status = status

    @property
    def seconds(self):
        """
        Returns: a float representing the amount of seconds it took to calculate the TestResult.
        Raises: TestError if type is not float or value is smaller than zero.
        """

        return self.__seconds

    @seconds.setter
    def seconds(self, seconds):
        if not (isinstance(seconds, float) or isinstance(seconds, int)):
            raise TestError("Please assign seconds of (float) integer to TestResult")
        if seconds < 0:
            raise TestError("Please assign seconds larger than zero to TestResult.")
        self.__seconds = seconds

    @property
    def expected_actual(self):
        """
        Returns: an instance of type ExpectedActual representing the expected and actual values of a TestResult.
        Raises: TestError if type is not ExpectedActual.
        """

        return self.__expected_actual

    @expected_actual.setter
    def expected_actual(self, expected_actual):
        if not isinstance(expected_actual, ExpectedActual) and expected_actual is not None:
            raise TestError("Please assign values of type ExpectedActual to TestResult.")
        self.__expected_actual = expected_actual

    @property
    def essential(self):
        """
        Returns: a boolean representing if a test is essential.
        Raises: TestError if type is not boolean.
        """

        return self.__essential

    @essential.setter
    def essential(self, essential):
        if not isinstance(essential, bool):
            raise TestError("Please assign essential as type boolean to TestResult.")
        self.__essential = essential

    @property
    def message(self):
        """
        Returns: a string representing the constructive feedback message when a test fails.
        Raises: TestError if type is not string.
        """

        return self.__message

    @message.setter
    def message(self, message):
        if not isinstance(message, str):
            raise TestError("Please assign message as type string to TestResult.")
        self.__message = message

    def __repr__(self):
        """
        Returns: a dictionary containing the attributes of a TestResult instance.
        """

        return {"name": self.name, "status": self.status.name, "seconds": self.seconds,
                "expected_actual": self.expected_actual.__repr__() if self.expected_actual is not None else None,
                "essential": self.essential,
                "message": self.message}
