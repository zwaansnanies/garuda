from enum import Enum

class Status(Enum):
    """
    An enum class to represent the status of a test.

    Values:
        pass : a test has passed.
        fail : a test has failed.
        ignored : a test was ignored.
        timeout : a test has timed out.
        skipped : a test was skipped.
    """

    PASS = 0
    FAIL = 1
    IGNORED = 2
    TIMEOUT = 3
    SKIPPED = 4
