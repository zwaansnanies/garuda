import sys
from garuda import TestFailureError, TestError, functools


def parse(s):
    if isinstance(s, str):
        s = s.strip()
        if s.lower() == "true":
            return True
        if s.lower() == "false":
            return False

        try:
            return int(s)
        except ValueError:
            pass

        try:
            return float(s)
        except ValueError:
            pass
    return s


def assert_equals(expected, actual, **kwargs):
    """
    Method that is used to determine whether the expected value is equal to the actual outcome.
    Args:
        expected: the value to be expected.
        actual: the actual value.
        **kwargs: a third parameter can be added to specify a precision (key-value pair) when dealing with floating point numbers.

    Returns: void if the expected value is equal to the actual value.
    Raises: TestFailureError if the expected value is not equal to the actual value or if both values are not of the same type.
    """
    if 'precision' in kwargs:
        precision = kwargs['precision']

        try:
            assert isinstance(expected, float)
            assert isinstance(actual, float)
            assert_equals(round(expected, precision), round(actual, precision))

        except AssertionError:
            raise TestFailureError(expected, actual)

    else:
        try:
            assert expected == actual
        except AssertionError:
            raise TestFailureError(expected, actual)


def compare_output_from_file(input_file: str, output_file: str, function):
    """
    Method that reads the input- and output-file line by line.
    Each input-line is passed to the given function which in turn returns an output-line.
    The generated output-line is afterwards compared to the corresponding output-line of the output_file-arg.

    Args:
        input_file: the input-file.
        output_file: the output-file.
        function: the function that evaluates each input-line to a corresponding output-line.

    Returns: void if each line from the output-file and each line evaluated by function is the same.
    Raises: TestFailureError if one line from the output-file and one line evaluated by function is not the same.
    """

    with open(input_file, "r") as input_f:
        with open(output_file, "r") as output_f:
            input_lines = input_f.readlines()
            output_lines = output_f.readlines()
            if len(input_lines) != len(output_lines):
                raise TestError("Input file({}) and output file({}) have different lengths.".format(len(input_lines),
                                                                                                    len(output_lines)))
            results = []

            for i in range(len(input_lines)):
                input_value = parse(input_lines[i])

                output_value = parse(output_lines[i])
                func_value = function(input_value)
                try:
                    assert_equals(output_value, func_value)
                except TestFailureError:
                    results.append((input_value, output_value, func_value, False))
                results.append((input_value, output_value, func_value, True))

            passed = functools.reduce((lambda acc, result: acc + 1 if result[3] else acc), results, 0)

            if any(list(map(lambda x: not x[3], results))):
                raise TestFailureError("100% passed", "{}% passed".format(round((passed / len(results) * 100), 2)))


def compare_output_from_function(input_file: str, solution_function, function):
    """
    Method that reads the input_file-arg line by line.
    Each input-line is passed to both given function and solution_function.
    The generated output-lines are afterwards compared to each other.

    Args:
        input_file: the input-file.
        solution_function: the solution-function that evaluates the actual result for each line.
        function: the function that evaluates the expected result for each line.

    Returns: void if each line evaluated by the solution-function and function is the same.
    Raises: TestFailureError if one line evaluated by the solution-function and function is not the same.
    """

    with open(input_file, "r") as input_f:
        input_lines = input_f.readlines()

        results = []

        for i in range(len(input_lines)):
            input_value = parse(input_lines[i])
            solution_value = solution_function(input_value)
            func_value = function(input_value)
            try:
                assert_equals(solution_value, func_value)
            except TestFailureError:
                results.append((input_value, solution_value, func_value, False))
            results.append((input_value, solution_value, func_value, True))

        passed = functools.reduce((lambda acc, result: acc + 1 if result[3] else acc), results, 0)

        if any(list(map(lambda x: not x[3], results))):
            raise TestFailureError("100% passed", "{}% passed".format(round((passed / len(results) * 100), 2)))


def assert_true(condition):
    """
    Checks whether the condition is true.
    Args:
        condition: a condition (boolean) to be evaluated.

    Returns: void if the condition is true.
    Raises: TestFailureError if the condition is false or if the condition is not of type bool.
    """

    assert_equals(True, condition)


def assert_false(condition):
    """
    Checks whether the condition is false.
    Args:
        condition: a condition (boolean) to be evaluated.

    Returns: void if the condition is false.
    Raises: TestFailureError if the condition is true or if the condition is not of type bool.
    """

    assert_equals(False, condition)


def assert_none(condition):
    """
    Checks whether the condition is None.

    Args:
        condition: the condition to be evaluated.

    Returns: void if the condition is None.
    Raises: TestFailureError if the condition is not None.
    """

    assert_equals(None, condition)


def assert_uses_function(function, function_to_use, args: tuple):
    """
    Checks whether a given function uses another function.
    Args:
        function: the function to be executed.
        function_to_use: the function that should be used by the given function-arg.
        args: the arguments to be passed to the function.

    Returns: void if function uses the function_to_use.
    Raises: TestFailureError if the function doesn't use the function_to_use.
    """

    def run_and_increase(*_args, **kwargs):
        req = kwargs["req"]
        param = kwargs["param"]
        counter = kwargs["counter"]
        counter[0] = counter[0] + 1
        return req(*param)

    counter = [0]
    module = sys.modules[function_to_use.__module__]
    orig_func = getattr(module, function_to_use.__name__)
    setattr(module,
            function_to_use.__name__, lambda *args: run_and_increase(param=args, req=orig_func, counter=counter))

    function(*args)
    if not counter[0]:
        raise TestFailureError("to call " + function_to_use.__name__, function_to_use.__name__ + " was not called")

    sys.modules[function_to_use.__module__] = module
