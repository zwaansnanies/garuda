from abc import abstractmethod

from garuda import Status


class ScoringStrategy:
    """
    An abstract class that needs to be subclassed to represent a scoring strategy for calculating the score for a
    collection of tests that have been run.
    """

    def __init__(self, test_group):
        """
        Args:
            test_group: an instance of type TestGroup.
        """

        self.test_group = test_group

    @property
    def name(self):
        """
        Returns: a string representing the name of a ScoringStrategy subclass.
        """

        strategy_name = self.__class__.__name__
        result = ""

        for char in strategy_name:
            if char.isupper():
                result += " " + char.lower()
            else:
                result += char

        return result.strip()

    @abstractmethod
    def __call__(self, *args, **kwargs):
        """
        An abstract method to override the call brackets '()', to be overridden by the ScoringStrategy subclass.
        """

        pass


class Cumulative(ScoringStrategy):
    """
    A class inheriting from class ScoringStrategy to represent a cumulative strategy for calculating the score for a
    collection of tests.
    """

    def __call__(self, *args, **kwargs):
        """
        A method to override the call brackets '()' to perform the cumulative scoring strategy for calculating the
        score for the tests in the instance of type TestGroup.

        Args:
            *args: (optional).
            **kwargs: (optional).

        Returns: an float representing the score for the tests that have been run.
        """

        if self.test_group.passed == 0 or self.test_group.has_failed_essential:
            return 0
        else:
            return round(
                self.test_group.score * (self.test_group.passed / (self.test_group.passed + self.test_group.failed)), 2)


class AllOrNothing(ScoringStrategy):
    """
    A class inheriting from class ScoringStrategy to represent an all-or-nothing strategy for calculating the score of a
    collection of tests.
    """

    def __call__(self, *args, **kwargs):
        """
        A method to override the call brackets '()' to perform the all-or-nothing scoring strategy for calculating the
        score for the tests in the instance of type TestGroup.

        Args:
            *args: (optional).
            **kwargs: (optional).

        Returns: an float representing the score for the tests that have been run.
        """

        if self.test_group.failed > 0 or self.test_group.ignored:
            return 0
        else:
            return self.test_group.score


class SkipRemaining(ScoringStrategy):
    """
    A class inheriting from class ScoringStrategy to represent a skip-remaining strategy for calculating the score for a
    collection of tests.
    """

    def __call__(self, *args, **kwargs):
        """
        A method to override the call brackets '()' to perform the skip-remaining scoring strategy for calculating the
        score for the tests in the instance of type TestGroup.

        Args:
            *args: (optional).
            **kwargs: (optional).

        Returns: a float representing the score for the tests that have been run.
        """

        n_passed = 0
        results = self.test_group.test_results
        for i in range(len(results)):
            result = results[i]
            if result.status == Status.PASS:
                n_passed += 1
            elif result.status in (Status.FAIL, Status.TIMEOUT):
                for j in range(i + 1, len(results)):
                    skipped = results[j]
                    skipped.status = Status.SKIPPED
                break

        if n_passed == 0:
            return 0
        else:
            return round(self.test_group.score * (n_passed / self.test_group.count_tests()), 2)
