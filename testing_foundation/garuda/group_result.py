class GroupResult:
    """
    A class representing the result of a TestGroup instance.
    """

    def __init__(self, total, passed, failed, ignored, skipped, seconds, score):
        """
        Args:
            total: an integer representing the total number of tests.
            passed: an integer representing the number of tests that passed.
            failed: an integer representing the number of tests that failed.
            ignored: an integer representing the number of tests that are ignored.
            skipped: an integer representing the number of tests that are skipped.
            seconds: a float representing the amount of seconds it took to calculate the result.
            score: a float representing the calculated score.
        """

        self.total = total
        self.passed = passed
        self.failed = failed
        self.ignored = ignored
        self.skipped = skipped
        self.seconds = seconds
        self.score = score

    def all_ignored(self):
        """
        Returns: a boolean representing if all tests in the TestGroup instance are ignored.
        """

        return self.ignored == self.total

    def ratio(self):
        """
        Returns: a float representing the percentage of tests passed of the total amount minus the ignored tests.
        """

        try:
            return round((self.passed / (self.total - self.ignored) * 100))
        except ZeroDivisionError:
            return 0

    def seconds_string(self):
        """
        Returns: a formatted string representing the seconds expired.
        """

        return "{} seconds expired".format(self.seconds)

    def ratio_passed_string(self):
        """
        Returns: a formatted string representing the percentage of tests passed.
        """

        return "{} passed ({}%)".format(self.passed, self.ratio())

    def ratio_failed_string(self):
        """
        Returns: a formatted string representing the percentage of tests failed.
        """

        return "{} failed ({}%)".format(self.failed, 100 - self.ratio())

    def ratio_ignored_string(self):
        """
        Returns: a formatted string representing the percentage of tests ignored.
        """

        return "{} ignored".format(self.ignored)

    def ratio_skipped_string(self):
        """
        Returns: a formatted string representing the percentage of tests skipped.
        """

        return "{} skipped".format(self.skipped)

    def __add__(self, other):
        """
        A method to overload the + symbol to add two instances of type GroupResult.

        Args:
            other: an instance of type GroupResult.

        Returns: an instance of type GroupResult.
        Raises: ValueError if other instance is not of type GroupResult.
        """

        if type(other) is GroupResult:
            return GroupResult(self.total + other.total,
                               self.passed + other.passed,
                               self.failed + other.failed,
                               self.ignored + other.ignored,
                               self.skipped + other.skipped,
                               self.seconds + other.seconds,
                               self.score + other.score)
        else:
            raise ValueError("You can only add another result to a result.")

    def __repr__(self):
        """
        Returns: a dictionary containing the attributes of a GroupResult instance.
        """

        return {"total": self.total, "passed": self.passed, "failed": self.failed, "ignored": self.ignored,
                "skipped": self.skipped, "seconds": self.seconds, "score": self.score.__repr__()}


class OverallResult:
    """
    A class representing the overall result of a collection of GroupResult instances.
    """

    def __init__(self, group_result: GroupResult):
        """
        Args:
            group_result: a GroupResult instance representing the total sum of a collection of GroupResults.
        """

        self.group_result = group_result

    def __getattr__(self, item):
        return getattr(self.group_result, item)

    def __repr__(self):
        """
        Returns: a representation of the total GroupResult instance.
        """

        return self.group_result.__repr__()
