import json
import sys
import os


class ConfigReader:
    """
    A class representing the default values for the loggers and the tests
    as well as the logic to read the configuration file.
    """

    default_html_logger = True
    default_console_logger = True
    default_json_logger = True
    default_score = 0
    default_timeout = 1.0

    filename = "config.json"

    @staticmethod
    def __set_loggers(loggers):
        ConfigReader.__set_default_console_logger(loggers)
        ConfigReader.__set_default_html_logger(loggers)
        ConfigReader.__set_default_json_logger(loggers)

    @staticmethod
    def __set_default_html_logger(loggers):
        try:
            html = loggers["html"]
            assert type(html) == bool
            ConfigReader.default_html_logger = html

        except KeyError:
            pass

        except AssertionError:
            pass

    @staticmethod
    def __set_default_console_logger(loggers):
        try:
            cons = loggers["console"]
            assert type(cons) == bool
            ConfigReader.default_console_logger = cons

        except KeyError:
            pass

        except AssertionError:
            pass

    @staticmethod
    def __set_default_json_logger(loggers):
        try:
            cons = loggers["json"]
            assert type(cons) == bool
            ConfigReader.default_json_logger = cons

        except KeyError:
            pass

        except AssertionError:
            pass

    @staticmethod
    def __set_default_score(test_defaults):
        try:
            score = test_defaults["score"]
            assert type(score) == int or type(score) == float
            assert score >= 0
            ConfigReader.default_score = score

        except KeyError:
            pass

        except AssertionError:
            pass

    @staticmethod
    def __set_default_timeout(test_defaults):
        try:
            timeout = test_defaults["timeout"]
            assert type(timeout) == int or type(timeout) == float
            assert timeout >= 0.1
            ConfigReader.default_timeout = timeout

        except KeyError:
            pass

        except AssertionError:
            pass

    @staticmethod
    def __set_test_defaults(test_defaults):
        ConfigReader.__set_default_score(test_defaults)
        ConfigReader.__set_default_timeout(test_defaults)

    @staticmethod
    def read():
        """
        Reads the configuration file and calls the setters to set the default values.
        The configuration file needs to be in the same directory as the test-file.
        Returns: void
        """

        file_path = os.path.abspath(os.path.dirname(sys.argv[0]))
        file_path = os.path.join(file_path, ConfigReader.filename)
        try:
            with open(file_path) as config_file:
                data = json.load(config_file)

                loggers = get_loggers(data)
                test_defaults = get_test_defaults(data)

                if loggers is not None:
                    ConfigReader.__set_loggers(loggers)

                if test_defaults is not None:
                    ConfigReader.__set_test_defaults(test_defaults)

        except FileNotFoundError:
            pass

        except KeyError:
            pass


def get_loggers(data):
    """
    Args:
        data: complete configuration data from json-file.

    Returns: the loggers-object that contains all default values for different loggers.
    """

    try:
        return data["loggers"]
    except KeyError:
        pass


def get_test_defaults(data):
    """
    Args:
        data: complete configuration data from json-file.

    Returns: the test-defaults-object that contains all default values for different test-evaluation attributes (e.g. score and timeout).
    """

    try:
        return data["test_defaults"]
    except KeyError:
        pass
