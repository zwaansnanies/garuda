import time
from threading import Thread
import functools

import colorama

from garuda.test_suite import TestSuite
from garuda.errors import TestError, TestFailureError, TestTimeOutError
from garuda.test_group import TestGroup
from garuda.test_result import TestResult, Status, ExpectedActual
from garuda.config import ConfigReader

colorama.init()


# https://stackoverflow.com/questions/21827874/timeout-a-python-function-in-windows
def timeout(time_out):
    """
    A decorator method to wrap an instance of a test function to allow a running test to time out after a certain
     amount of time.

    Args:
        time_out: an integer representing the number of seconds after which a test times out.

    Returns: an instance of a function wrapped in a timeout function.

    Raises: TypeError if test function receives incorrect type. RuntimeError if test fails or is ignored. Exception if test does not complete before timeout time.
    """

    def deco(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            res = [TestTimeOutError()]

            def new_func():
                try:
                    res[0] = func(*args, **kwargs)
                except TypeError as e:
                    res[0] = e
                except RuntimeError as e:
                    res[0] = e

            t = Thread(target=new_func)
            t.daemon = True

            try:
                t.start()
                t.join(time_out)
            except Exception as je:
                raise je

            ret = res[0]
            if isinstance(ret, BaseException):
                raise ret

            return ret

        return wrapper

    return deco


def init_ignore(**kwargs):
    """
    Args:
        **kwargs: a dictionary containing the key-value pairs passed to the test decorator.

    Returns: a boolean representing if a test is ignored.
    Raises: TestError if ignore value is not of type boolean.
    """

    if "ignore" in kwargs:
        ignore = kwargs["ignore"]
        if not isinstance(ignore, bool):
            raise TestError("An ignore value should be a boolean.")
    else:
        ignore = False
    return ignore


def init_time_out(**kwargs):
    """
    Args:
        **kwargs: a dictionary containing the key-value pairs passed to the test decorator.

    Returns: a integer representing the number of seconds after which a test times out.
    Raises: TestError if time out value is not of type integer.
    """

    if "time_out" in kwargs:
        time_out = kwargs["time_out"]
        if not isinstance(time_out, int):
            raise TestError("A time-out value should be an int.")
        if not time_out > 0:
            raise TestError("A time-out value should be greater than zero.")
    else:
        time_out = ConfigReader.default_timeout
    return time_out


def init_essential(**kwargs):
    """
    Args:
        **kwargs: a dictionary containing the key-value pairs passed to the test decorator.

    Returns: a boolean representing if a test is essential.
    Raises: TestError is value of essential is not of type boolean.
    """

    if "essential" in kwargs:
        essential = kwargs["essential"]
        if not isinstance(essential, bool):
            raise TestError("An essential value should be a boolean.")
    else:
        essential = False
    return essential


def init_test_name(test_function, **kwargs):
    """
    Args:
        test_function: a function object representing the test function to be run.
        **kwargs: a dictionary containing the key-value pairs passed to the test decorator.

    Returns: a string instance representing the name of the test.
    Raises: TestError if name value is not of type string.
    """

    if "name" in kwargs:
        test_name = kwargs["name"]
        if not isinstance(test_name, str):
            raise TestError("Please assign a name of type string to your test.")
        if test_name.strip() == "":
            raise TestError("Please assign a non empty string to your test")
    else:
        test_name = test_function.__name__
    return test_name


def add_test_result_to_suite_or_group(test_result, suite_or_group):
    """
    Args:
        test_result: an instance of type TestResult.
        suite_or_group: an instance of type TestSuite or TestGroup.

    Returns: void
    """

    if isinstance(suite_or_group, TestGroup):
        suite_or_group.add_test_result(test_result)
    elif isinstance(suite_or_group, TestSuite):
        suite_or_group.add_test_result(test_result)


def init_suite_or_group(*args):
    """
    Args:
        *args: a list containing values passed to the test decorator.

    Returns: an instance of type TestSuite or TestGroup.
    Raises: TestError if no value is passed.
    """
    try:
        suite_or_group = args[0]
    except IndexError:
        raise TestError("Please assign a correct TestGroup- or TestSuite-object to your test.") from None
    return suite_or_group


def init_ignore_all(suite_or_group):
    """
    Args:
        suite_or_group: an instance of type TestSuite or TestGroup.

    Returns: a boolean representing if all tests are ignored.
    """

    if isinstance(suite_or_group, TestGroup):
        return suite_or_group.ignore_all
    return False


def test(*args, **kwargs):
    """
    A decorator method to wrap test functions.
    Args:
        *args: a list of values passed to the test decorator.
        **kwargs: a dictionary of key-value pairs passed to the test decorator.

    Returns: a function object representing a test function wrapped in a test decorator function.
    """

    ignore = init_ignore(**kwargs)
    time_out = init_time_out(**kwargs)
    essential = init_essential(**kwargs)

    def test_wrapper(test_function):
        test_function = timeout(time_out)(test_function)
        test_name = init_test_name(test_function, **kwargs)

        suite_or_group = init_suite_or_group(*args)
        ignore_all = init_ignore_all(suite_or_group)

        if not ignore and not ignore_all:
            start = time.time()
            try:
                test_function()
                end = time.time()
                result = TestResult(test_name,
                                    Status.PASS,
                                    end - start,
                                    essential=essential)

            except TestFailureError as e:
                end = time.time()
                result = TestResult(test_name,
                                    Status.FAIL,
                                    end - start,
                                    ExpectedActual(e.expected, e.actual),
                                    essential=essential)
            except TypeError as e:
                end = time.time()
                result = TestResult(test_function.__name__,
                                    Status.FAIL,
                                    end - start,
                                    essential=essential,
                                    message=f'{str(e)} -> Check # arguments and their types. If this is not the issue'
                                            f', there is a TypeError in your code')

            except TestTimeOutError:
                result = TestResult(test_name,
                                    Status.TIMEOUT,
                                    time_out,
                                    essential=essential,
                                    message='timed out')

        else:
            result = TestResult.default_ignored(test_name)

        add_test_result_to_suite_or_group(result, suite_or_group)
        return result

    return test_wrapper
