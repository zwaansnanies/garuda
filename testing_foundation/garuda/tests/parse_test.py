import unittest

from garuda import parse


class TestParse(unittest.TestCase):

    def test_True_as_string(self):
        self.assertEqual(parse("True"), True, "Should be True as boolean")
        self.assertEqual(parse("true"), True, "Should be True as boolean")

    def test_False_as_string(self):
        self.assertEqual(parse("False"), False, "Should be False as boolean")
        self.assertEqual(parse("false"), False, "Should be False as boolean")

    def test_int_as_string(self):
        self.assertEqual(parse("1"), 1, "Should be 1 as int")

    def test_float_as_string(self):
        self.assertEqual(parse("0.1"), 0.1, "Should be 0.1 as float")

    def test_float(self):
        self.assertEqual(parse(0.1), 0.1, "Should be 0.1 as float")

    def test_true(self):
        self.assertEqual(parse(True), True, "Should be True as boolean")

    def test_false(self):
        self.assertEqual(parse(False), False, "Should be True as boolean")

    def test_int(self):
        self.assertEqual(parse(5), 5, "Should be 5 as int")


if __name__ == '__main__':
    unittest.main()
