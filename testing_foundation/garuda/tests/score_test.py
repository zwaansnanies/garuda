import unittest

from garuda import Score


class TestScore(unittest.TestCase):

    def test_ratio_add(self):
        tr = Score(5, 10)
        tr2 = Score(10, 15)
        tr3 = tr + tr2
        self.assertEqual(tr3.achieved, 15, "Should be 15")
        self.assertEqual(tr3.total, 25, "Should be 25")
