import unittest
from garuda import GroupResult


class GroupResultTest(unittest.TestCase):

    def test_ratio_half(self):
        gr = GroupResult(2, 1, 1, 0, 0, 1, 10)
        self.assertEqual(gr.ratio(), 50, "Should be 50%")

    def test_ratio_third(self):
        gr = GroupResult(3, 1, 2, 0, 0, 1, 10)
        self.assertEqual(gr.ratio(), 33, "Should be 33%")

    def test_ratio_zero(self):
        gr = GroupResult(2, 0, 2, 0, 0, 1, 10)
        self.assertEqual(gr.ratio(), 0, "Should be 0%")

    def test_ratio_100(self):
        gr = GroupResult(2, 2, 0, 0, 0, 1, 10)
        self.assertEqual(gr.ratio(), 100, "Should be 100%")

    def test_ratio_half_with_ignored(self):
        gr = GroupResult(3, 1, 1, 1, 0, 1, 10)
        self.assertEqual(gr.ratio(), 50, "Should be 50%")

    def test_ratio_third_with_ignored(self):
        gr = GroupResult(4, 1, 2, 1, 0, 1, 10)
        self.assertEqual(gr.ratio(), 33, "Should be 33%")

    def test_ratio_zero_with_ignored(self):
        gr = GroupResult(3, 0, 2, 1, 0, 1, 10)
        self.assertEqual(gr.ratio(), 0, "Should be 0%")

    def test_ratio_100_with_ignored(self):
        gr = GroupResult(3, 2, 0, 1, 0, 1, 10)
        self.assertEqual(gr.ratio(), 100, "Should be 100%")

    def test_all_ignored(self):
        gr = GroupResult(3, 0, 0, 3, 1, 0, 10)
        self.assertEqual(gr.all_ignored(), True, "Should be all ignored (True)")

    def test_all_ignored_returns_false(self):
        gr = GroupResult(3, 1, 0, 2, 1, 0, 10)
        self.assertEqual(gr.all_ignored(), False, "Should not be all ignored (False)")

    def test_add_group_results(self):
        gr = GroupResult(3, 1, 1, 1, 0, 1, 10)
        gr2 = GroupResult(5, 2, 2, 1, 0, 2, 10)

        gr3 = gr + gr2
        self.assertEqual(gr3.total, 8, "Should not be all ignored (False)")
        self.assertEqual(gr3.passed, 3, "Should not be all ignored (False)")
        self.assertEqual(gr3.failed, 3, "Should not be all ignored (False)")
        self.assertEqual(gr3.ignored, 2, "Should not be all ignored (False)")
        self.assertEqual(gr3.seconds, 3, "Should not be all ignored (False)")
        self.assertEqual(gr3.score, 20, "Should not be all ignored (False)")


if __name__ == '__main__':
    unittest.main()
