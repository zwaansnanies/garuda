from garuda.logger import *
import json


class SetEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)


class JSONLogger(Logger):
    """
    A class that is used to log the test results in JSON.
    """

    @staticmethod
    def __to_json(test):
        return json.dumps(test, cls=SetEncoder)

    def passed_test(self, test_result):
        """
        Returns a JSON-string of a passed test.

        Args:
            test_result: a TestResult-instance.

        Returns: a JSON-string.
        """

        return self.__to_json(test_result)

    def failed_test(self, test_result):
        """
        Returns a JSON-string of a failed test.

        Args:
            test_result: a TestResult-instance.

        Returns: a JSON-string.
        """

        return self.__to_json(test_result)

    def timeout_test(self, test_result):
        """
        Returns a JSON-string of a timeout test.

        Args:
            test_result: a TestResult-instance.

        Returns: a JSON-string.
        """

        return self.__to_json(test_result)

    def ignored_test(self, test_result):
        """
        Returns a JSON-string of an ignored test.

        Args:
            test_result: a TestResult-instance.

        Returns: a JSON-string.
        """

        return self.__to_json(test_result)

    def skipped_test(self, test_result):
        """
        Returns a JSON-string of a skipped test.

        Args:
            test_result: a TestResult-instance.

        Returns: a JSON-string.
        """

        return self.__to_json(test_result)

    def group_result_with__right_representation(self, group_result):
        """
        Returns a JSON-string of a a group result.

        Args:
            group_result: a GroupResult-instance.

        Returns: a JSON-string.
        """

        return group_result.__repr__()

    def overall_result_with_right_representation(self, overall_result):
        """
        Returns a JSON-string of the overall result.

        Args:
            overall_result: an OverallResult-instance.

        Returns: a JSON-string.
        """

        return overall_result.__repr__()

    @staticmethod
    def __file_to_write_path():
        argv = sys.argv[0]
        path = os.path.dirname(argv)
        if not os.path.exists(path + "\\json"):
            os.mkdir(path + "\\json")
        filename = str(os.path.basename(argv).split(".")[0]) + ".json"
        path = os.path.abspath(path)
        path += f"\\json\\{filename}"
        return path

    def __write_to_file(self, json):
        try:
            with open(self.__file_to_write_path(), "w+") as file:
                file.write(json)
        except FileNotFoundError:
            os.mkdir("json")
            with open(self.__file_to_write_path(), "w+") as file:
                file.write(json)

    def log(self):
        """
        Generates the complete JSON-structure with the test results, the group results and the overall result.
        Thereafter, it will write the JSON to the associated file.

        Returns: void
        """

        log_results_object = {}
        log_results_object["test_groups"] = []

        for log_element in self.log_elements:
            log_element_object = {}

            test_group: TestGroup = log_element.test_group
            group_result: GroupResult = log_element.group_result

            log_element_object["name"] = test_group.name
            log_element_object["test_group"] = test_group.__repr__()
            log_element_object["group_result"] = self.group_result_with__right_representation(group_result)

            log_results_object["test_groups"].append(log_element_object)

        log_results_object["overall_result"] = self.overall_result_with_right_representation(self.overall_result)

        json = self.__to_json(log_results_object)
        self.__write_to_file(json)
