from garuda import *
from garuda.logger import Logger
from termcolor import colored


class ConsoleLogger(Logger):
    """
    A class that is used to print the test results on the console.
    """

    @staticmethod
    def __dotted_line():
        return "".join("." for _ in range(100))

    @staticmethod
    def __blue(text):
        return colored(text, 'blue')

    @staticmethod
    def __yellow(text):
        return colored(text, 'yellow')

    @staticmethod
    def __green(text):
        return colored(text, 'green')

    @staticmethod
    def __red(text):
        return colored(text, 'red')

    @staticmethod
    def __white(text):
        return colored(text, 'white')

    def passed_test(self, test_result):
        """
        Returns a formatted string of a passed test in the color green.
        Args:
            test_result: a TestResult-instance.

        Returns: a formatted string.
        """

        return self.__green(test_result)

    def failed_test(self, test_result):
        """
        Returns a formatted string of a failed test in the color red.
        Args:
            test_result: a TestResult-instance.

        Returns: a formatted string.
        """

        return self.__red(test_result)

    def timeout_test(self, test_result):
        """
        Returns a formatted string of a timeout test in the color red.
        Args:
            test_result: a TestResult-instance.

        Returns: a formatted string.
        """

        return self.__red(test_result)

    def ignored_test(self, test_result):
        """
        Returns a formatted string of an ignored test in the color yellow.
        Args:
            test_result: a TestResult-instance.

        Returns: a formatted string.
        """

        return self.__yellow(test_result)

    def skipped_test(self, test_result):

        """
        Returns a formatted string of a skipped test in the color white.
        Args:
            test_result: a TestResult-instance.

        Returns: a formatted string.
        """

        return self.__white(test_result)

    def group_result_with__right_representation(self, group_result):
        """
        Returns a formatted string of the group result. The number of passed tests (and ratio) are marked in green.
        The total score is displayed in green or red (in case of an (in)sufficient score).
        Args:
            group_result: a GroupResult-instance.

        Returns: a formatted string.
        """

        group_result: GroupResult
        return self.__green(group_result.ratio_passed_string()) + "\n" + \
               self.__score_with_color(group_result.score) + "\n" + self.__dotted_line()

    def overall_result_with_right_representation(self, overall_result):
        """
        Returns a formatted string of the overall result. The total number of tests performed are marked in blue.
        The number of passed tests (and ratio) are marked in green, the number of failed tests (and ratio) are marked
        in red, the number of ignored tests are marked in yellow and the number of skipped tests are marked in white.
        Below that is the total time that has expired in grey and the total score in green or red (in case of an
        (in)sufficient score).

        Args:
            overall_result: an OverallResult-instance.

        Returns: a formatted string.
        """

        return self.__blue(f"{overall_result.total} TESTS:") + "\n" + \
               self.__green(overall_result.ratio_passed_string()) + "\n" + \
               self.__red(overall_result.ratio_failed_string()) + "\n" + \
               self.__yellow(overall_result.ratio_ignored_string()) + "\n" + \
               self.__white(overall_result.ratio_skipped_string()) + "\n\n" + \
               self.__white(overall_result.seconds_string()) + "\n" + \
               self.__score_with_color(overall_result.score)

    def __score_with_color(self, score: Score):
        if score.total == 0:
            return self.__blue(score)

        ratio = score.achieved / score.total
        if ratio < 0.5:
            return self.__red(score)
        else:
            return self.__green(score)

    def log(self):
        """
        Returns a complete formatted string with the test results, the group results and the overall result.
        Returns: a formatted string.
        """

        for log_element in self.log_elements:
            log_element: LogElement

            test_group: TestGroup = log_element.test_group
            group_result: GroupResult = log_element.group_result

            print(self.__blue(f"{test_group.name} ({test_group.strategy.name})"))
            for test_result in test_group.test_results:
                print(self.test_result_with_right_representation(test_result))

            print()
            print(self.group_result_with__right_representation(group_result))
            print()

        print(self.__white("Overall result"))
        overall_result: OverallResult = self.overall_result
        print(self.overall_result_with_right_representation(overall_result))
        print()
