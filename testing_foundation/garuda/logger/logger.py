from garuda import *
from abc import abstractmethod


class Logger:
    """
    An abstract class that describes a way to log test results.
    """

    def __init__(self):
        self.log_elements = []
        self.overall_result = None

    def send_log_element(self, log_element):
        """
        Adds a LogElement-instance to a list of log elements.

        Args:
            log_element: a LogElement-instance.

        Returns: void
        """

        self.log_elements.append(log_element)

    def send_overall_result(self, overall_result):
        """
        Adds an OverallResult-instance to a list of overall results.

        Args:
            overall_result: an OverallResult-instance.

        Returns: void
        """

        self.overall_result = overall_result

    @abstractmethod
    def passed_test(self, test_result):
        """
        An abstract method that represents an implementation of a passed test.

        Args:
            test_result: a TestResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    @abstractmethod
    def failed_test(self, test_result):
        """
        An abstract method that represents an implementation of a failed test.

        Args:
            test_result: a TestResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    @abstractmethod
    def timeout_test(self, test_result):
        """
        An abstract method that represents an implementation of a timeout test.

        Args:
            test_result: a TestResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    @abstractmethod
    def ignored_test(self, test_result):
        """
        An abstract method that represents an implementation of an ignored test.

        Args:
            test_result: a TestResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    @abstractmethod
    def skipped_test(self, test_result):
        """
        An abstract method that represents an implementation of a skipped test.

        Args:
            test_result: a TestResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    def test_result_with_right_representation(self, test_result):
        """
        Calls the right method (passed_test, failed_test, ...) depending on the status of the test result.

        Args:
            test_result: a TestResult-instance.

        Returns: depends on the subclass implementation.
        """

        status = test_result.status
        if status == Status.PASS:
            return self.passed_test(test_result)
        elif status == Status.FAIL:
            return self.failed_test(test_result)
        elif status == Status.TIMEOUT:
            return self.timeout_test(test_result)
        elif status == Status.IGNORED:
            return self.ignored_test(test_result)
        elif status == Status.SKIPPED:
            return self.skipped_test(test_result)

    @abstractmethod
    def group_result_with__right_representation(self, group_result):
        """
        An abstract method that represents an implementation of a group result.

        Args:
            group_result: a GroupResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    @abstractmethod
    def overall_result_with_right_representation(self, overall_result):
        """
        An abstract method that represents an implementation of an overall result.

        Args:
            overall_result: an OverallResult-instance.

        Returns: depends on the subclass implementation.
        """

        pass

    @abstractmethod
    def log(self):
        """
        An abstract method that represents a log-implementation.
        Returns: depends on the subclass implementation
        """

        pass
