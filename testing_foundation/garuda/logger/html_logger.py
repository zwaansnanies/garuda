import os
import sys

from garuda import *
from garuda.logger import Logger
from yattag import *


class HTMLLogger(Logger):
    """
    A class that is used to log the test results in HTML.
    """

    def __init__(self):
        super().__init__()
        doc, tag, text = Doc().tagtext()
        self.doc = doc
        self.tag = tag
        self.text = text

    def passed_test(self, test_result):
        """
        Generates HTML of a passed test and adds it to the doc-variable.

        Args:
            test_result: a TestResult-instance.

        Returns: void
        """

        with self.tag("div", klass="test_pass"):
            with self.tag("div", klass="test_title"):
                self.text(test_result.name)

    def failed_test(self, test_result):
        """
        Generates HTML of a failed test and adds it to the doc-variable.

        Args:
            test_result: a TestResult-instance.

        Returns: void
        """

        values = test_result.expected_actual
        message = test_result.message
        with self.tag("div", klass="test_fail"):
            with self.tag("div", klass="test_title"):
                self.text(test_result.name)
            if values is not None:
                with self.tag("div", klass="expected_actual"):
                    self.__wrap_new_lines_in_div(visualize(values.expected, values.actual))
            if message != '':
                with self.tag("div", klass="message"):
                    with self.tag("div", klass="line"):
                        self.text("message: " + message)

    def timeout_test(self, test_result):
        """
        Generates HTML of a timeout test and adds it to the doc-variable.

        Args:
            test_result: a TestResult-instance.

        Returns: void
        """

        with self.tag("div", klass="test_fail"):
            with self.tag("div", klass="test_title"):
                self.text(test_result.name)

            with self.tag("div", klass="line"):
                self.text("timed out")

    def ignored_test(self, test_result):
        """
        Generates HTML of an ignored test and adds it to the doc-variable.

        Args:
            test_result: a TestResult-instance.

        Returns: void
        """

        with self.tag("div", klass="test_ignore"):
            with self.tag("div", klass="test_title"):
                self.text(test_result.name)

            with self.tag("div", klass="line"):
                self.text("ignored")

    def skipped_test(self, test_result):
        """
        Generates HTML of a skipped test and adds it to the doc-variable.

        Args:
            test_result: a TestResult-instance.

        Returns: void
        """

        with self.tag("div", klass="test_skip"):
            with self.tag("div", klass="test_title"):
                self.text(test_result.name)

            with self.tag("div", klass="line"):
                self.text("skipped")

    @staticmethod
    def __style_path():
        return os.path.join(os.path.dirname(__file__), 'style.css')

    @staticmethod
    def __file_to_write_path():
        argv = sys.argv[0]
        path = os.path.dirname(argv)
        if not os.path.exists(path+"\\html"):
            os.mkdir(path+"\\html")
        filename = str(os.path.basename(argv).split(".")[0]) + ".html"
        path = os.path.abspath(path)
        path += f"\\html\\{filename}"
        return path

    def __style(self):
        with open(self.__style_path(), "r") as file:
            style = file.read()
        return style

    def __write_to_file(self):
        try:
            with open(self.__file_to_write_path(), "w+") as file:
                file.write(self.doc.getvalue())
        except FileNotFoundError:
            os.mkdir("html")
            with open(self.__file_to_write_path(), "w+") as file:
                file.write(self.doc.getvalue())


    def __wrap_new_lines_in_div(self, string: str):
        lines = string.split('\n')
        with self.tag("div", klass="line"):
            for line in lines:
                self.text(line)
                self.doc.stag("br")

    def group_result_with__right_representation(self, group_result):
        """
        Generates HTML of a group result and adds it to the doc-variable.

        Args:
            group_result: GroupResult-instance.

        Returns: void
        """

        with self.tag("div", klass="group_results"):
            with self.tag("div", klass="line"):
                self.text(group_result.ratio_passed_string())
            with self.tag("div", klass="line"):
                self.text(str(group_result.score))

    def overall_result_with_right_representation(self, overall_result):
        """
        Generates HTML of the overall result and adds it to the doc-variable.

        Args:
            overall_result: OverallResult-instance.

        Returns: void
        """

        overall_result: OverallResult
        with self.tag("div", klass="overall_results"):
            with self.tag("div", klass="testgroup_title"):
                self.text("Overall results")
            with self.tag("div", klass="line"):
                self.text("{} TESTS: ".format(overall_result.total))
            with self.tag("div", klass="line"):
                self.text(overall_result.ratio_passed_string())
            with self.tag("div", klass="line"):
                self.text(overall_result.ratio_failed_string())
            with self.tag("div", klass="line"):
                self.text(overall_result.ratio_ignored_string())
            with self.tag("div", klass="line"):
                self.text(overall_result.ratio_skipped_string())
            with self.tag("div", klass="line"):
                self.text(overall_result.seconds_string())
            with self.tag("div", klass="line large"):
                self.text(str(overall_result.score))

    def log(self):
        """
        Generates the complete HTML-structure with the test results, the group results and the overall result and adds
        it to the doc-variable.
        Thereafter, it will write the HTML to the associated file.

        Returns: void
        """

        self.doc.asis('<!DOCTYPE html>')

        with self.tag('html'):
            with self.tag('head'):
                with self.tag('style'):
                    self.text(self.__style())

            with self.tag('body'):
                self.overall_result_with_right_representation(self.overall_result)

                for log_element in self.log_elements:
                    log_element: LogElement

                    test_group: TestGroup = log_element.test_group
                    group_result: GroupResult = log_element.group_result

                    with self.tag("div", klass="testgroup"):
                        with self.tag("div", klass="testgroup_title"):
                            self.text(test_group.name)

                            with self.tag("div", klass="line small"):
                                self.text(test_group.strategy.name)

                        for test_result in test_group.test_results:
                            self.test_result_with_right_representation(test_result)

                        self.group_result_with__right_representation(group_result)

        self.__write_to_file()
