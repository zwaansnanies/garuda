from garuda.logger import ConsoleLogger, HTMLLogger
from garuda.config import ConfigReader
from garuda.logger.json_logger import JSONLogger


class LoggerTracker:
    """
    A class used to keep up with the different kind of loggers
    """

    @staticmethod
    def __read_loggers_from_json():
        loggers = []
        html = ConfigReader.default_html_logger
        console = ConfigReader.default_console_logger
        json = ConfigReader.default_json_logger

        if html:
            loggers.append(HTMLLogger())
        if console:
            loggers.append(ConsoleLogger())
        if json:
            loggers.append(JSONLogger())

        return loggers

    def __init__(self):
        self.tracker = []
        self.add_trackers()

    def send_log_element(self, log_element):
        """
        Adds a LogElement-instance to all different kind of loggers kept by the tracker.

        Args:
            log_element: the logElement-instance to be added.

        Returns: void
        """

        for logger in self.tracker:
            logger.send_log_element(log_element)

    def send_overall_result(self, overall_result):
        """
        Adds an OverallResult-instance to all different kind of loggers kept by the tracker.

        Args:
            overall_result: the OverallResult-instance to be added.

        Returns: void
        """

        for logger in self.tracker:
            logger.send_overall_result(overall_result)

    def log(self):
        """
        Calls the log-method of all the different kind of loggers kept by the tracker.
        Returns: void
        """

        for logger in self.tracker:
            logger.log()

    def add_trackers(self):
        """
        Adds all loggers that are specified in the config.json-file.
        Returns: void
        """

        loggers = self.__read_loggers_from_json()
        for logger in loggers:
            self.tracker.append(logger)

    def __enter__(self):
        """
        Creates a LoggerTracker-instance that can be used within a context manager.
        Returns: a LoggerTracker-instance
        """

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Method that will be called when leaving the context manager. It calls the log-method.
        Returns: void
        """

        self.log()
