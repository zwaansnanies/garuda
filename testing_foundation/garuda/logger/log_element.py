class LogElement:
    """
    A class used to represent a test group with its belonging group result.
    """

    def __init__(self, group_result, test_group):
        """
        Args:
            group_result: an instance of a group result.
            test_group: an instance of a test group.
        """

        self.group_result = group_result
        self.test_group = test_group

    def __repr__(self):
        """
        Returns: a dictionary of the LogElement-instance.
        """

        return {"group_result": self.group_result, "test_group": self.test_group}
