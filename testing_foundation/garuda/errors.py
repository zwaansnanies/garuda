class TestFailureError(RuntimeError):
    """
    A class to represent a TestFailureError. A TestFailure is thrown when the expected value is not equal to the actual
    value.
    """

    def __init__(self, expected, actual):
        """
        Args:
            expected: the value to be expected.
            actual: the actual value.
        """
        super().__init__()
        self.expected = expected
        self.actual = actual


class TestTimeOutError(RuntimeError):
    """
    A class to represent a TestTimeOutError. A TestTimeOutError is thrown when the running test is out of time.
    """

    def __init__(self):
        super().__init__()


class TestError(RuntimeError):
    """
    A class to represent a TestError. A TestError is thrown by improper use of the framework.
    """

    def __init__(self, message):
        """
        Args:
            message: the message that gives more information about the reason of the error.
        """

        super().__init__(message)
