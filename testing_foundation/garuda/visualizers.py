def visualize(expected, actual):
    """
    Method to visualize the difference between the expected and actual value.
    Based on their type, the generated output will be constructive so that the user knows where the mistakes were made.

    Args:
        expected: the value of the test that was expected.
        actual: the value of the test that was calculated.

    Returns: the output of the called function if the type is supported, else it will output the default string for the expected and actual value.
    """
    if not _check_type(expected, actual):
        result = f'(expected type: {type(expected).__name__}, actual type: {type(actual).__name__})'
    elif type(expected) is list:
        result = visualize_list(expected, actual)
    elif type(expected) is dict:
        result = visualize_dict(expected, actual)
    elif type(expected) is set:
        result = visualize_set(expected, actual)
    elif type(expected) is float:
        result = visualize_float(expected, actual)
    elif type(expected) is tuple:
        result = visualize_tuple(expected, actual)
    else:
        result = f'(expected: {expected}, actual: {actual})'

    return result


def visualize_list(expected: list, actual: list):
    """
    Method to visualize lists. This will properly visualize lists as well as two-dimensional lists.
    This will also list the mistakes in dimensions and types of the elements in the lists.

    Args:
        expected: the value of the test that was expected.
        actual: the value of the test that was calculated.

    Returns: String of the expected list and actual list with the errors that relate to the dimensions and the types of the elements of the lists.
    """

    result = ''

    if len(expected) != len(actual):
        result += '(expected dimension is {}, actual dimension is {})\n'.format(len(expected), len(actual))

    for i in range(len(expected)):
        if isinstance(expected[i], list):
            if not isinstance(actual[i], list):
                result += f'(expected type on row {i} is {type(expected[i]).__name__}' \
                          f', actual type was {type(actual[i]).__name__})\n'\

            if len(expected[i]) != len(actual[i]):
                result += f'(expected dimension on row {i} is {len(expected[i])}' \
                          f', actual dimension was {len(actual[i])})\n'\

        elif not isinstance(actual[i], type(expected[i])):
            result += f'(expected type on row {i} is {type(expected[i]).__name__},' \
                      f' actual type was {type(actual[i]).__name__})\n'

    exp_visual = _stringify_list(expected)
    act_visual = _stringify_list(actual)
    result += f'\n(expected: {exp_visual}\nactual: {act_visual})\n'
    return result


def visualize_dict(expected: dict, actual: dict):
    """
    Method to visualize dictionaries. This will return which key-value pairs were correct, which were incorrect and
    what they should have been, which keys were missing and which keys were unnecessary in the actual set.

    Args:
        expected: the value of the test that was expected.
        actual: the value of the test that was calculated.

    Returns: String of the dictionary that contains missing, correct, incorrect and unnecessary items. This will also return the overall result of the expected and actual result.
    """

    rest_map_expected = {}
    rest_map_actual = {}
    correct_items = {}
    incorrect_items = {}
    should_be_items = {}

    for k, v in expected.items():
        if k not in actual:
            rest_map_expected[k] = v
        else:
            if expected[k] == actual[k]:
                correct_items[k] = v
            else:
                should_be_items[k] = v
                incorrect_items[k] = actual[k]

    for k, v in actual.items():
        if k not in expected:
            rest_map_actual[k] = v

    result = ''
    result += f'(expected: {expected}, actual: {actual})\n\n'
    result += f'\tcorrect_values: {correct_items}\n'
    if len(incorrect_items) != 0:
        result += f'\tincorrect_values: {incorrect_items} -> should be: {should_be_items}\n'
    else:
        result += f'\tincorrect_values: {incorrect_items}'
    result += f'\tmissing_keys: {rest_map_expected}\n'
    result += f'\tunnecessary_keys: {rest_map_actual}'

    return result


def visualize_set(expected: set, actual: set):
    """
    Method to visualize sets. This will return which items in the set were correct and which ones were missing as well
    as which ones were unnecessary in the actual set.
    Args:
        expected: the value of the test that was expected.
        actual: the value of the test that was calculated.

    Returns: String of the set containing correct, missing and unnecessary items. This will also return the overall result of the expected and actual set.
    """

    missing_items = set()
    rest_items = set()
    correct_items = set()

    for v in expected:
        if v not in actual:
            missing_items.add(v)
        else:
            correct_items.add(v)

    for v in actual:
        if v not in expected:
            rest_items.add(v)

    result = ''
    result += f'(expected: {expected}, actual: {actual})\n\n'
    result += f'\tcorrect_items: {correct_items}\n'
    result += f'\tmissing_items: {missing_items}\n'
    result += f'\tunnecessary_items: {rest_items}\n'

    return result


def visualize_float(expected: float, actual: float):
    """
    Method to visualize. This will return the expected and actual value of the test. The difference in precision in both
    float values is also shown to the user
    Args:
        expected: the value of the test that was expected
        actual: the value of the test that was calculated

    Returns: String with expected and actual float of the test
    as well as the difference in precision of the float-values

    """
    precision_exp = _precision(expected)
    precision_act = _precision(actual)

    result = ''
    result += f'(expected: {expected}, actual: {actual})\n'
    if precision_exp != precision_act:
        result += f'(expected precision: {precision_exp}, actual precision: {precision_act})\n'

    return result


def visualize_tuple(expected: tuple, actual: tuple):
    """
    Method to visualize tuples. This will return the expected tuple and the actual tuple as well as the difference
    in length
    Args:
        expected: the value of the test that was expected
        actual: the value of the test that was calculated

    Returns: String with expected and actual tuple of the test. The difference in length in both tuples is also shown
    to the user

    """
    result = f'(expected: {expected}, actual: {actual})\n'
    if len(expected) != len(actual):
        result += f'(length expected tuple: {len(expected)}, length actual tuple: {len(actual)})\n'

    return result


def _check_type(x, y):
    if type(x) != type(y):
        return False
    return True


def _stringify_list(xs):
    result = '\n'
    for i in range(len(xs)):
        if type(xs[i]) == list:
            itm_result = ''
            for j in range(len(xs[i])):
                itm_result += ' [ ' + str(xs[i][j]) + ' ] '
            result += itm_result + '\n'
        else:
            result += ' [ ' + str(xs[i]) + ' ] \n'

    return result


def _precision(x: float):
    string = str(x)
    parts = string.split('.')

    if len(parts) == 2:
        return len(parts[1])
