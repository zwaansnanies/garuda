class Score:
    """
    A class to represent the score of a collection of tests.
    """

    def __init__(self, achieved, total):
        """
        Args:
            achieved: an integer to represent the number of tests that have passed.
            total: an integer to represent the total number of tests that have been run.
        """

        self.achieved = achieved
        self.total = total

    def __add__(self, other):
        """
        A method to overload the + symbol so two instances of type score can be added.

        Args:
            other: an instance of type score.

        Returns: an instance of type score.
        """

        return Score(self.achieved + other.achieved, self.total + other.total)

    def __str__(self):
        """
        Returns: a string representing the instance of type score.
        """

        return "{}/{}".format(self.achieved, self.total) if self.total != 0 else "No score given"

    def __repr__(self):
        """
        Returns: a dictionary containing the attributes of an instance of type score.
        """

        return {"achieved": self.achieved, "total": self.total}
