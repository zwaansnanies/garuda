// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as os from "os";
import { basename, dirname, extname, join } from "path";
let _terminal: vscode.Terminal;
let _TERMINAL_DEFAULT_SHELL_WINDOWS: string | null = null;
let _codeFile: string;
let _document: vscode.TextDocument;
let _config: vscode.WorkspaceConfiguration;
let _cwd: string | undefined = ".";
let _workspaceFolder: string;
const TmpDir = os.tmpdir();


let fs = require("fs");
var path = require('path');
let panel: vscode.WebviewPanel;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	initialize();


	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "garuda-extension" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json

	let showgarudatests = vscode.commands.registerCommand('extension.showgarudatests', () => {
		// Create and show a new webview
		panel = vscode.window.createWebviewPanel(
			'showgarudatests', // Identifies the type of the webview. Used internally
			'Garuda Tests', // Title of the panel displayed to the user
			vscode.ViewColumn.One, // Editor column to show the new webview panel in.
			{} // Webview options. More on these later.
		);




		var filePath = vscode.window.activeTextEditor?.document.fileName;
		if (filePath !== undefined) {
			loadHtml(panel, filePath);
		}

	});

	let generategarudaconfig = vscode.commands.registerCommand('extension.generategarudaconfig', () => {
		// Create and show a new webview
		vscode.window.showInformationMessage("Generating config file!");

		let config: string = `{
			\"loggers\": {
				\"console\": true,
				\"html\": false,
				\"json\": true
			},
			\"test_defaults\": {
				\"timeout\": 1.0,
				\"score\": 0
			}
		}`;

		fs.writeFile(vscode.workspace.rootPath + '/config.json', config, function (err: any) {
			if (err) {
				return console.error(err);
			}
			console.log("File created!");
		});

	});

	let generategarudasample = vscode.commands.registerCommand('extension.generategarudasampletests', async () => {
		// Create and show a new webview
		vscode.window.showInformationMessage("Generating sample tests!");
		let result = await vscode.window.showInputBox({
			value: 'sample',
			placeHolder: 'sample'
		});
		if (result !== undefined) {
			if (result === "") {
				result = "sample";
			}
			fs.writeFile(vscode.workspace.rootPath + '/' + result + '.py', sampletests, function (err: any) {
				if (err) {
					return console.error(err);
				}
				console.log("File created!");
			});

		}


	});

	let generategarudabigsample = vscode.commands.registerCommand('extension.generateallgarudasampletests', async () => {
		// Create and show a new webview
		vscode.window.showInformationMessage("Generating sample tests!");
		let result = await vscode.window.showInputBox({
			value: 'sample',
			placeHolder: 'sample'
		});
		if (result !== undefined) {
			if (result === "") {
				result = "sample";
			}
			fs.writeFile(vscode.workspace.rootPath + '/' + result + '.py', bigsampletests, function (err: any) {
				if (err) {
					return console.error(err);
				}
				console.log("File created!");
			});

		}


	});

	function loadHtml(panel: vscode.WebviewPanel, filePath: string) {
		if (panel) {

			const folder = filePath?.split("\\").slice(0, -1).join("\\");
			const fileName = filePath?.split("\\")[filePath?.split("\\").length - 1];
			const filePathn: vscode.Uri = vscode.Uri.file(path.join(folder, 'html', fileName?.split('.')[0] + '.html'));
			vscode.window.showInformationMessage(filePathn.fsPath);
			panel.webview.html = fs.readFileSync(filePathn.fsPath, 'utf8');

		}
	}


	let rungarudatests = vscode.commands.registerCommand('extension.rungarudatests', () => {
		vscode.window.showInformationMessage('Running tests!');
		const cp = require('child_process');
		var filePath = vscode.window.activeTextEditor?.document.fileName;
		const folder = filePath?.split("\\").slice(0, -1).join("\\");
		const fileName = filePath?.split("\\")[filePath?.split("\\").length - 1];
		runInTerminal('python ' + fileName);
		if (panel && filePath) {
			loadHtml(panel, filePath);

		}


	});


	context.subscriptions.push(rungarudatests);
	context.subscriptions.push(showgarudatests);
	context.subscriptions.push(generategarudaconfig);
	context.subscriptions.push(generategarudasample);
	context.subscriptions.push(generategarudabigsample);

}

function getWorkspaceFolder(): string {
	if (vscode.workspace.workspaceFolders) {
		if (_document) {
			const workspaceFolder = vscode.workspace.getWorkspaceFolder(_document.uri);
			if (workspaceFolder) {
				return workspaceFolder.uri.fsPath;
			}
		}
		return vscode.workspace.workspaceFolders[0].uri.fsPath;
	} else {
		return "";
	}
}

function initialize(): void {
	_config = getConfiguration("code-runner");
	_cwd = _config.get<string>("cwd");
	if (_cwd) {
		return;
	}
	_workspaceFolder = getWorkspaceFolder();
	var filePath = vscode.window.activeTextEditor?.document.fileName;
	if (filePath === undefined) {
		filePath = "";
	}
	_cwd = dirname(filePath);

	if (_cwd) {
		return;
	}
	_cwd = TmpDir;
}

// this method is called when your extension is deactivated
export function deactivate() { }

async function runInTerminal(executor: string) {


	let isNewTerminal = false;
	_terminal = vscode.window.createTerminal("Code");
	isNewTerminal = true;

	_terminal.show(true);
	//this.sendRunEvent(executor, true);
	executor = changeExecutorFromCmdToPs(executor);
	let command = getFinalCommandToRunCodeFile(executor, true);
	command = changeFilePathForBashOnWindows(command);
	if (!isNewTerminal) {
		await vscode.commands.executeCommand("workbench.action.terminal.clear");
	}
	if (_cwd === undefined) {
		_cwd = ".";
	}
	if (true) {
		const cwd = changeFilePathForBashOnWindows(_cwd);
		_terminal.sendText(`cd "${cwd}"`);
	}
	_terminal.sendText(executor);
}

function changeFilePathForBashOnWindows(command: string): string {
	if (os.platform() === "win32") {
		const windowsShell = vscode.workspace.getConfiguration("terminal").get<string>("integrated.shell.windows");
		const terminalRoot = _config.get<string>("terminalRoot");
		if (windowsShell && terminalRoot) {
			command = command
				.replace(/([A-Za-z]):\\/g, (match, p1) => `${terminalRoot}${p1.toLowerCase()}/`)
				.replace(/\\/g, "/");
		} else if (windowsShell && windowsShell.toLowerCase().indexOf("bash") > -1 && windowsShell.toLowerCase().indexOf("windows") > -1) {
			command = command.replace(/([A-Za-z]):\\/g, replacer).replace(/\\/g, "/");
		}
	}
	return command;
}


function replacer(match: string, p1: string): string {
	return `/mnt/${p1.toLowerCase()}/`;
}

function changeExecutorFromCmdToPs(executor: string): string {
	if (os.platform() === "win32") {
		let windowsShell = vscode.workspace.getConfiguration("terminal").get<string>("integrated.shell.windows");
		if (windowsShell === null) {
			windowsShell = getTerminalDefaultShellWindows();
		}
		if (windowsShell && windowsShell.toLowerCase().indexOf("powershell") > -1 && executor.indexOf(" && ") > -1) {
			let replacement = "; if ($?) {";
			executor = executor.replace("&&", replacement);
			replacement = "} " + replacement;
			executor = executor.replace(/&&/g, replacement);
			executor = executor.replace(/\$dir\$fileNameWithoutExt/g, ".\\$fileNameWithoutExt");
			return executor + " }";
		}
	}
	return executor;
}

function getTerminalDefaultShellWindows(): string {
	if (!_TERMINAL_DEFAULT_SHELL_WINDOWS) {
		const isAtLeastWindows10 = os.platform() === "win32" && parseFloat(os.release()) >= 10;
		const is32ProcessOn64Windows = process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432");
		const powerShellPath =
			`${process.env.windir}\\${is32ProcessOn64Windows ? "Sysnative" : "System32"}\\WindowsPowerShell\\v1.0\\powershell.exe`;
		_TERMINAL_DEFAULT_SHELL_WINDOWS = isAtLeastWindows10 ? powerShellPath : getWindowsShell();
	}
	return _TERMINAL_DEFAULT_SHELL_WINDOWS;
}


function getWindowsShell(): string {
	return process.env.comspec || "cmd.exe";
}

function getCodeFileWithoutDirAndExt(): string {
	const regexMatch = _codeFile.match(/.*[\/\\](.*(?=\..*))/);
	return regexMatch ? regexMatch[1] : _codeFile;
}

function getConfiguration(section?: string): vscode.WorkspaceConfiguration {
	if (_document) {
		return vscode.workspace.getConfiguration(section, _document.uri);
	} else {
		return vscode.workspace.getConfiguration(section);
	}
}

function getCodeFileDir(): string {
	const regexMatch = _codeFile.match(/(.*[\/\\]).*/);
	return regexMatch ? regexMatch[1] : _codeFile;
}

function quoteFileName(fileName: string): string {
	return '\"' + fileName + '\"';
}

function getCodeBaseFile(): string {
	const regexMatch = _codeFile.match(/.*[\/\\](.*)/);
	return regexMatch ? regexMatch[1] : _codeFile;
}

function getDriveLetter(): string {
	const regexMatch = _codeFile.match(/^([A-Za-z]:).*/);
	return regexMatch ? regexMatch[1] : "$driveLetter";
}

function getFinalCommandToRunCodeFile(executor: string, appendFile: boolean = true): string {
	let cmd = executor;

	if (_codeFile) {
		const codeFileDir = getCodeFileDir();
		let pythonPath = getConfiguration("python").get<string>("pythonPath");
		if (pythonPath === undefined) {
			pythonPath = "python";
		}
		const placeholders: Array<{ regex: RegExp, replaceValue: string }> = [
			// A placeholder that has to be replaced by the path of the folder opened in VS Code
			// If no folder is opened, replace with the directory of the code file
			{ regex: /\$workspaceRoot/g, replaceValue: getWorkspaceRoot(codeFileDir) },
			// A placeholder that has to be replaced by the code file name without its extension
			{ regex: /\$fileNameWithoutExt/g, replaceValue: getCodeFileWithoutDirAndExt() },
			// A placeholder that has to be replaced by the full code file name
			{ regex: /\$fullFileName/g, replaceValue: quoteFileName(_codeFile) },
			// A placeholder that has to be replaced by the code file name without the directory
			{ regex: /\$fileName/g, replaceValue: getCodeBaseFile() },
			// A placeholder that has to be replaced by the drive letter of the code file (Windows only)
			{ regex: /\$driveLetter/g, replaceValue: getDriveLetter() },
			// A placeholder that has to be replaced by the directory of the code file without a trailing slash
			{ regex: /\$dirWithoutTrailingSlash/g, replaceValue: quoteFileName(getCodeFileDirWithoutTrailingSlash()) },
			// A placeholder that has to be replaced by the directory of the code file
			{ regex: /\$dir/g, replaceValue: quoteFileName(codeFileDir) },
			// A placeholder that has to be replaced by the path of Python interpreter
			{ regex: /\$pythonPath/g, replaceValue: pythonPath },
		];

		placeholders.forEach((placeholder) => {
			cmd = cmd.replace(placeholder.regex, placeholder.replaceValue);
		});
	}

	return (cmd !== executor ? cmd : executor + (appendFile ? " " + quoteFileName(_codeFile) : ""));
}

function getCodeFileDirWithoutTrailingSlash(): string {
	return getCodeFileDir().replace(/[\/\\]$/, "");
}

function getWorkspaceRoot(codeFileDir: string): string {
	return _workspaceFolder ? _workspaceFolder : codeFileDir;
}


let sampletests: string = `
from garuda import *

with TestSuite() as ts:
  
	@test(ts)
	def sample_test_1():
		assert_false(False)

	with TestGroup(ts, name="is_prime", score=100, ignore=False, strategy=Cumulative) as tg:

		@test(tg)
		def sample_test_2():
			assert_true(True)

		@test(tg, ignore=True)
		def sample_test_3():
			assert_false(False)

		for i in range(3):

			@test(tg, name=f'sample_test_for_loop_{i}')
			def sample_test_4():
				x = i * 2
				assert_equals(x, i * 2)
`;

let bigsampletests: string = `
from garuda import *

with TestSuite() as ts:

	@test(ts)
	def my_function():
		assert_equals(True, True)

	with TestGroup(ts, name="my_group") as tg:

		@test(tg)
		def my_function2():
			assert_equals(True, True)
		
		@test(tg, name="my_custom_name_test")
		def my_function3(time_out=1):
			while True:
				assert_equals(True, True)
	
	with TestGroup(ts, name="my_group", score=100, strategy=Cumulative) as tg:

		kv = {"one": 1, "two": 2, "three": [3]}
		kv_ref = {"two": 2, "one": 1, "three": 3}

		@test(tg)
		def map_test():
				assert_equals(kv_ref, kv)

		set_1 = {1, 2, 3}

		ref = {1, 2, 1, 1, 5, 6}

		@test(tg)
		def set_test():
				assert_equals(set_1, ref)
		
		@test(tg)
		def testje():
			assert_equals(True, True)

	with TestGroup(ts, name="my_group", score=101, strategy=AllOrNothing) as tg:
		@test(tg)
		def testje2():
			assert_equals(True, True)
		
		@test(tg)
		def testje3():
			assert_equals(True, True)

	with TestGroup(ts, name="my_group", score=10, strategy=SkipRemaining) as tg:
		@test(tg)
		def testje4():
			assert_equals(True, True)

		@test(tg)
		def testje5():
			assert_equals(True, False)

		@test(tg, ignored=True)
		def testje6():
			assert_equals(True, False)
		
		@test(tg, name="This test will not run")
		def testje7():
			assert_equals(True, True)
`;